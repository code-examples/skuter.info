# [skuter.info](https://skuter.info) source codes

<br/>

### Run skuter.info on localhost

    # vi /etc/systemd/system/skuter.info.service

Insert code from skuter.info.service

    # systemctl enable skuter.info.service
    # systemctl start skuter.info.service
    # systemctl status skuter.info.service

http://localhost:4054
